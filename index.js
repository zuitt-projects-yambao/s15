// alert("test");



function oddEvenChecker(num) {
	if (num % 2 === 0) {
		return `This number ${num} is even`;
	}
	else if (num % 2 == true) {
		return `This number ${num} is odd`;
	}

	else
		alert("invalid Input");
}

let number1 = oddEvenChecker(46);
console.log(number1);

let number2 = oddEvenChecker(105);
console.log(number2);


function budgetChecker(budget){
	if(typeof budget == "number"){
		if(budget > 40000){
			return "You are over the budget.";
		}
		else
			return "You have resources left.";
	}

	else
		alert("Invalid Input");
}

let budgetMsg1 = budgetChecker(5);
console.log("budgetChecker(5)");
console.log(budgetMsg1);

let budgetMsg2 = budgetChecker(45000);
console.log("budgetChecker(45000)");
console.log(budgetMsg2);